Project Alpha

Feature 1
* [x] Fork and clone the starter project from Project Alpha 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djhtml
* [x] Use pip freeze to generate a requirements.txt file
* [x] test

Feature 2
* [x] Create a Django project named tracker so that the manage.py file is in the top directory
    ```django-admin startproject tracker .   ```
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
    ```  python manage.py startapp accounts  ```
    ```  INSTALLED_APPS = [
    "accounts.apps.AccountsConfig",  # replace with your
     ...                             # app's name   ```
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
     ```  python manage.py startapp projects  ```
    ```  INSTALLED_APPS = [
    "projects.apps.ProjectsConfig",  # replace with your
     ...                             # app's name   ```
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
      ```  python manage.py startapp tasks  ```
    ```  INSTALLED_APPS = [
    "tasks.apps.TasksConfig",  # replace with your
     ...                             # app's name   ```
* [x] Run the migrations
    ``` python manage.py makemigrations ```
    ``` python manage.py migrate ```
* [x] Create a super user
    ``` python manage.py makesuperuser ```
* [x] Test

Feature 3
* [x] The Project model should have the following attributes.
``` class Project(models.Model) ```
``` name = models.CharField(max_length=200) ```
``` description = models.TextField() ```
``` members = models.ManyToManyField(User method...,          ````related_name="projects") ```
``` def __str__(self): ```
 ```return self.name ```

* [x] Make migrations and migrate your database.
* [x] Testing

Feature 4

* [x] Register the Project model with the admin so that you can see it in the Django admin site.
* [x] Test


Feature 5
* [x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
* [x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
* [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
* [x] Create a template for the list view that complies with the following specifications
* [x] Test

Feature 6
* [x] Redirect from "" to list view in tracker urls.py , register to "home"
* [x] Test

Feature 7
* [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
* [x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
* [x] Create a templates directory under accounts
* [x] Create a registration directory under templates
* [x] Create an HTML template named login.html in the registration directory
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
* [x] Test
  
Feature 8
* [x] Protect the list view for the Project model so that only a person that has logged in can access it
* [x] Change the queryset of the view to filter the Project objects where members equals the logged in user
* [x] Test

Feature 9
* [x] In the accounts/urls.py file import the LogoutView from the same module that you imported the LoginView from
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
* [x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
* [x] Test

Feature 10
* [x] You'll need to import the UserCreationForm from the built-in auth forms 
* [x] You'll need to use the special create_user  method to create a new user account from their username and password
* [x] You'll need to use the login  function that logs an account in
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] Test

Feature 11
* [x] Create task model with attributes
* [x] The Task model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Make migrations and migrate your database.
* [x] Test

Feature 12
* [x] Register the Task model with the admin so that you can see it in the Django admin site.
* [x] Test

Feature 13
* [x] Create a view that shows the details of a particular project
* [x] A user must be logged in to see the view
* [x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
* [x] Create a template to show the details of the project and a table of its tasks
* [x] Update the list template to show the number of tasks for a project
* [x] Update the list template to have a link from the project name to the detail view for that project
* [x] Test

Feature 14
* [x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
* [x] A person must be logged in to see the view
* [x] If the project is successfully created, it should redirect to the detail page for that project
* [x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
* [x] Create an HTML template that shows the form to create a new Project (see the template specifications below)
* [x] Add a link to the list view for the Project that navigates to the new create view
* [x] Test

Feature 15
* [x] Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
* [x] The view must only be accessible by people that are logged in
* [x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
* [x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
* [x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
* [x] Create a template for the create view that complies with the following specifications
* [x] Add a link to create a task from the project detail page that complies with the following specifications
* [x] Test

Feature 16
* [x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
* [x] The view must only be accessible by people that are logged in
* [x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
* [x] Create an HTML template that conforms with the following specification
* [x] Test

Feature 17
* [x] Create an update view for the Task model that only is concerned with the is_completed field
* [x] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
* [x] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* [x] Modify the "My Tasks" view to comply with the template specification
* [x] Test

Feature 18
* [x] Download markdownify
* [x] Run markdownify in projects details
* [x] test

Feature 19
* [x] Add nav to pages
* [x] test

!!COMPLETED!!