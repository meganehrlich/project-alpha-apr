from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import CreateForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


def redirect_view(request):
    response = redirect("list_projects")
    return response


@login_required
def show_project(request, pk):
    projects = Project.objects.get(pk=pk)
    context = {"projects": projects}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    context = {}
    form = CreateForm(request.POST or None)
    if form.is_valid():
        avar = form.save()
        return redirect("show_project", pk=avar.pk)

    context["form"] = form
    return render(request, "projects/create.html", context)
