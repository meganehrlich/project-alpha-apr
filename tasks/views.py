from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from projects.views import list_projects
from .models import Task


@login_required
def create_task(request):
    context = {}
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(list_projects)

    context["form"] = form
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/showtasks.html", context)


@login_required
def complete_task(request, pk):
    complete = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        complete.is_completed = False
        complete.is_completed = True
        complete.save()
        return redirect("/tasks/mine/", pk=pk)

    return render(request, "tasks/create.html", {"complete": complete})
